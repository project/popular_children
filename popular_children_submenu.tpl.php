<?php

/**
 * @file
 * Default implementation of the popular_children_submenu hook.
 *
 * Available variables:
 * - $title: The title of the list.
 * - $display_mode: The default display mode for the submenu. Can be one of the
 *   following: `'link'`, `'teaser'`, `'full'`. Note that individual items may
 *   be provided as links anyway if it's not possible to render them as nodes.
 * - $elements: An array of elements to add to the list. Each element contains:
 *   + 'class': The class to add to the list item,
 *   + 'data':  The rendered item.
 * - $attributes_string: The attributes to add to the list.
 */
?>

<?php if ($title): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul<?php print $attributes_string; ?>>
  <?php foreach($elements as $element): ?>
    <li class="<?php print $element['class']; ?>"><?php print $element['data']; ?></li>
  <?php endforeach; ?>
</ul>
