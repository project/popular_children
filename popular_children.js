/**
 * @file
 * Browser-side behaviour of the module.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.popularChildrenLinks = {
    attach: function (context, settings) {
      function linkHandler(e) {
        // Get the menu link ID.
        var mlid = $(this).parents('[data-pc-external]').attr('data-pc-mlid');

        // Save the data.
        $.ajax({
          type: 'POST',
          url: '/popular_children/click_logger',
          data: {
            mlid: mlid,
            token: Drupal.settings.popularChildren.token
          }
        });
      }

      // The links in menus that we marked with data-pc-external attribute.
      $('[data-pc-external] a').click(linkHandler);
    }
  };
}(jQuery));
