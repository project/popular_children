<?php

/**
 * @file
 * API functions for the module.
 */

/**
 * @defgroup popular_children_api_links API: Getting links
 * @{
 * Functions enabling other modules to get the list of popular links.
 */

/**
 * Alter a node loaded for display in Popular Items submenu.
 *
 * @param stdObject $node
 *   The node that will be displayed in the submenu.
 * @param array $link
 *   The link loaded from the database that points to the node.
 */
function hook_popular_submenu_node_alter(&$node, $link) {
  if (strpos($link['link_path'], 'my_module_path') !== FALSE) {
    $node = my_module_load_object($link['link_path']);
  }
}

/**
 * @} End of "popular_children_api_links".
 */


/**
 * @defgroup popular_children_api_links API: Getting links
 * @{
 * Functions enabling other modules to get the list of popular links.
 */

/**
 * Provides the select options for menu items available for the module.
 */
function popular_children_mlid_menu_options() {
  return _popular_children_menu_items();
}

/**
 * Returns the list of popular links.
 *
 * @param string $miid
 *   The menu item identifier for the links to relate to, in format
 *   'menu-name:mlid' (same as returned from
 *   popular_children_mlid_menu_options()).
 * @param string $type
 *   (Optional)One of the POPULAR_CHILDREN_BLOCK_TYPE_ constants determining
 *   whether to count the children of the reference item specified by miid, or
 *   its siblings.
 * @param bool $role_filter
 *   (Optional)TRUE to enable role-specific counting, and FALSE to disable.
 * @param int $max
 *   (Optional)The maximum number of links to return.
 *
 * @return array
 *   The ordered array of fully loaded menu link arrays.
 */
function popular_children_get_links($miid, $type = POPULAR_CHILDREN_BLOCK_TYPE_CHILDREN, $role_filter = FALSE, $max = 5) {
  $menu_info = explode(':', $miid);
  if (count($menu_info) != 2) {
    return array();
  }

  return _popular_children_popular_links(array(
    'menu_name' => $menu_info[0],
    'mlid' => $menu_info[1],
    'type' => $type,
    'role_filter' => $role_filter,
    'delta' => 'popular_children_api',
    'max_links' => $max,
  ));
}

/**
 * @} End of "popular_children_api_links".
 */
