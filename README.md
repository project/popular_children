Popular Children
================

INTRODUCTION
------------

This module logs the page views in specified menus, and enables creation of
special blocks listing the most popular items selected from children or siblings
of a particular menu item. The menu item to which each block is related can be
fixed, or configured to show links related to user's current position in the
menu.

It is possible to provide listings more tailored to each user by enabling role
filtering for blocks - in that configuration, the block will count the link
popularity based on number of visits made by users whose roles match the roles
of the user for whom the popularity is calculated.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

CONFIGURATION
-------------

### Configuring permissions ###
  - **Administer Popular Children** permission allows modifying module
	 configuration,
  - **Configure Popular Content blocks** permission allows creating and
	 configuring Popular Content blocks.

### Global module configuration ###

Visit
[Popular Children Configuration](/admin/config/user-interface/popular_children)
to configure the menus used to measure popularity, the lengths of the logging
period, and the cache.

* Menus to log

  Tick the boxes next to the menus which you intend to use for calculating
  popular items. The menus that are ticked will be available to use when
  creating Popular Content blocks.

* Logging period

  This field lets you configure how long to store the clicks on the menus for.
  The logged clicks on the menu items from before that time period will be
  removed on regular basis.

* Caching length

  This module uses a custom caching system. You can use this field to specify
  how often the Popular Content blocks refresh their links.

* Clear logs

  If you want to remove all recorded activity in the watched menus, tick this
  box before submitting the form.

### Adding blocks ###

Go to [Administration » Structure » Blocks](/admin/structure/block), and click
on the 'Add a Popular Content Block' link on the top. On the following page,
configure the following:

   * Block name

     This name is shown on the block list. It also constitutes the default title
     for the block.

   * Block Type

     Select whether the block should display the most popular items amongst the
     *children* of the related menu item, or its *siblings*.

   * Filter by role

     This module saves the role information when users click on the links. If
     this box is ticked, only the clicks made by members of current user's roles
     will count towards the popularity of displayed links. You can use it to
     create lists of interesting links that are better tailored for each user.

   * Maximum number of links

     This field simply specifies how many links to show in the block. If there
     is not enough clicks logged in the database, smaller number of links will
     be shown.

   * Relative To

     This field tells the block where to look for popular links. In conjunction
     with the **Block Type** field, either the siblings, or the children of the
     selected menu item will be included in the click count.

     In addition to each menu item in each of the menus the module is watching
     (configured in admin/config/user-interface/popular_children), you can also
     select 'all items' in a menu, or 'Current item'. Selecting 'all items' will
     make the block show the most popular amongst **all** of the items in that
     menu. Selecting 'Current item' will display the list of popular links
     relative to where the block is displayed - if it is shown on a node
     included in the selected menu, it will show links related to that menu
     item.
